## Tasks

An ETL task represents the migration of data records of a particular type (news articles, image files, user accounts, etc.) from one data representation to another. It is the controller tying together all the other components of the Soong ETL framework.

Many configuration scenarios will require multiple ETL tasks executed in a particular sequence. They may also require steps which do not fit the extract-transform-load model, and which it would be convenient for ETL tools to incorporate. We thus have a lower-level `TaskInterface` to handle the basics of being a task, while operations specific to ETL operations are in `EtlTaskInterface`.

Tasks may implement multiple operations, including `status`, `rollback`, `analyze` - but the primary operation of interest is `migrate`. An ETL task will behave as follows:

1. The task constructs the configured `ExtractorInterface`, which obtains data from a source such as a SQL query, a CSV file, an XML/JSON API, etc.
2. Iterating over the extractor returns one `DataRecordInterface` (collection of named `DataPropertyInterface` instances) at a time containing source data. The task creates an empty `DataRecordInterface` representing the destination data.
3. The task configuration defines a `transform` pipeline keyed by destination property names. For each of these properties, a sequence of one or more `TransformerInterface` classes with corresponding configuration is invoked - usually, the first one will be configured to accept one or more source property names, and the results will be fed to subsequent transformers, with the final result assigned to the named property in the destination `DataRecordInterface`.
4. The destination `DataRecordInterface` is passed to the configured `LoaderInterface` to be loaded into the destination store - a SQL database, a CSV file, etc.
5. If an optional `KeyMapInterface` is configured within the task, it is used to store the mapping from the source record's unique key to the destination record's unique key. This enables keyed relationships to be maintained even if keys change when migrating, as well as enabling rollback.


```
interface TaskInterface
{

    /**
     * Execute the named operation for the task.
     *
     * @param string $operation
     *   Name of the operation, which is a method on the class.
     * @todo Do better than to just name a method here.     *
     * @param array $options
     *   List of options affecting execution of the operation.
     *
     * @return mixed
     * @todo Should return some sort of status
     */
    public function execute(string $operation, array $options = []);

    /**
     * Return the task's configuration as an array.
     *
     * @return array
     */
    public function getConfiguration() : array;

    /**
     * Indicate whether dependent tasks may proceed.
     *
     * @return bool
     *   TRUE if this task has no outstanding records to process, else FALSE.
     */
    public function isCompleted(): bool;

    /**
     * Retrieve the specified task.
     *
     * @param $id
     *
     * @return \Soong\Task\TaskInterface
     */
    public static function getTask(string $id) : TaskInterface;

    /**
     * Create a task object with a given id from provided configuration.
     *
     * @param string $id
     * @param array $configuration
     *
     * @return null
     */
    public static function addTask(string $id, array $configuration);
}
```

```
interface EtlTaskInterface extends TaskInterface
{

    /**
     * @return \Soong\KeyMap\KeyMapInterface
     */
    public function getKeyMap() : ?KeyMapInterface;

    /**
     * @return \Soong\Extractor\ExtractorInterface
     */
    public function getExtractor(): ExtractorInterface;

    /**

    /**
     * Get the operation currently running for this task.
     */
    public function getCurrentOperation();
}
```
